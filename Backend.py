from flask import Flask, request, jsonify
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import joblib
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report
from sklearn.pipeline import Pipeline
import json
import torch
from transformers import BertTokenizer, BertModel
from sklearn.metrics.pairwise import cosine_similarity
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

def load_model_predict():
    return joblib.load('model.pkl')



modelpredict = load_model_predict()

train_data = None

@app.route('/dataprocessing', methods=['POST'])
def dataprocessing():
    global model
    try:
        file = request.files['file']
        data = json.load(file)

        def check_and_impute_missing_values(df):
            if df.isnull().values.any():
                df.fillna('', inplace=True)  

        df = pd.DataFrame(data)

        check_and_impute_missing_values(df)

        X = df[['titre', 'résumé', 'expériences', 'compétences', 'secteur']]
        y = df['besoins']

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        for col in ['expériences', 'compétences']:
            X_train[col] = X_train[col].apply(lambda x: '; '.join(x) if isinstance(x, list) else x)
            X_test[col] = X_test[col].apply(lambda x: '; '.join(x) if isinstance(x, list) else x)
        
        y_train_str = y_train.apply(lambda x: ', '.join(x) if isinstance(x, list) else x)
        y_test_str = y_test.apply(lambda x: ', '.join(x) if isinstance(x, list) else x)

        X_train_combined = X_train['titre'] + ' ' + X_train['résumé'] + ' ' + X_train['expériences'] + ' ' + X_train['compétences'] + ' ' + X_train['secteur']
        X_test_combined = X_test['titre'] + ' ' + X_test['résumé'] + ' ' + X_test['expériences'] + ' ' + X_test['compétences'] + ' ' + X_test['secteur']

        model = Pipeline([
            ('tfidf', TfidfVectorizer()), 
            ('clf', RandomForestClassifier(n_estimators=100, random_state=42))
        ])

        model.fit(X_train_combined, y_train_str)

        predictions = model.predict(X_test_combined)

        accuracy = accuracy_score(y_test_str, predictions)
        report = classification_report(y_test_str, predictions)

        joblib.dump(modelpredict, 'modelpredict.pkl')

        return jsonify({
            'message': 'Data preprocessing and model training completed',
            'accuracy': accuracy,
            'classification_report': report
        }), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@app.route('/predict', methods=['POST'])
def predict():
    try:

        file_path = 'prediction_tree_dataset.json'
        with open(file_path, 'r', encoding='utf-8') as f:
            data = json.load(f)


        df = pd.DataFrame(data)


        print("Colonnes présentes dans le DataFrame:", df.columns.tolist())
        

        def check_and_impute_missing_values(df):
            if df.isnull().values.any():
                df.fillna('', inplace=True)  

        check_and_impute_missing_values(df)


        expected_columns = ['nom', 'titre', 'résumé', 'expériences', 'publications', 'compétences', 'secteur']
        for col in expected_columns:
            if col not in df.columns:
                return jsonify({'error': f"Missing column in data: '{col}'"}), 400


        for col in ['expériences', 'compétences']:
            df[col] = df[col].apply(lambda x: '; '.join(x) if isinstance(x, list) else x)


        X_combined = df['titre'] + ' ' + df['résumé'] + ' ' + df['expériences'] + ' ' + df['compétences'] + ' ' + df['secteur']


        pretrained_model = load_model_predict()


        predictions = pretrained_model.predict(X_combined)

        df['predictions'] = predictions


        result = []
        for index, row in df.iterrows():
            result.append({
                "nom": row.get('nom', ""),
                "titre": row.get('titre', ""),
                "résumé": row.get('résumé', ""),
                "expériences": row.get('expériences', ""),
                "publications": row.get('publications', ""),
                "compétences": row.get('compétences', ""),
                "secteur": row.get('secteur', ""),
                "besoins": row.get('predictions', "").split(', ')
            })

        return result
    except Exception as e:
        return jsonify({'error': str(e)}), 400





def reformat__freelance_input_data(data):
    services_titles = [service["title"] for service in data["services"]]
    services_sectors = list(set([service["sector"] for service in data["services"]]))

    missions_titles = [mission["title"] for mission in data["missions"]]
    missions_descriptions = [mission["description"] for mission in data["missions"]]

    historique_projets = ". ".join(missions_descriptions)

    formatted_data = [
        {
            "historique_projets": historique_projets,
            "missions_proposées": ", ".join(services_titles),
            "secteur_activité": ", ".join(services_sectors)
        }
    ]

    return formatted_data



@app.route('/matching', methods=['POST'])
def train_matching_model():
    try:
        freelance_data = request.json
        freelances_data = reformat__freelance_input_data(freelance_data)

        clients_data = predict()

        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        model = BertModel.from_pretrained('bert-base-uncased')

        def get_embeddings(text):
            inputs = tokenizer(text, return_tensors='pt', truncation=True, padding=True, max_length=512)
            outputs = model(**inputs)
            return outputs.last_hidden_state.mean(dim=1).detach().numpy()

        freelance_texts = [f"{freelance['historique_projets']} {freelance['missions_proposées']} {freelance['secteur_activité']}" for freelance in freelances_data]
        client_texts = []
        for client in clients_data:
            résumé = client.get('résumé', "")
            expériences = client.get('expériences', "")
            compétences = ', '.join(client.get('compétences', []))
            secteur = client.get('secteur', "")
            besoins = ' '.join(client.get('besoins', []))

            client_texts.append(f"{résumé} {expériences} {compétences} {secteur} {besoins}")

        freelance_embeddings = np.array([get_embeddings(text) for text in freelance_texts])
        client_embeddings = np.array([get_embeddings(text) for text in client_texts])

        similarities = cosine_similarity(freelance_embeddings.reshape(len(freelances_data), -1), client_embeddings.reshape(len(clients_data), -1))

        similarity_threshold = 0.7
        similar_clients_profiles = []

        for i, freelance_data in enumerate(freelances_data):
            similar_clients = []

            for j, client_data in enumerate(clients_data):
                if similarities[i][j] >= similarity_threshold:
                    similar_client_info = {
                        'résumé': client.get('résumé', ""),
                        'expériences': client.get('expériences', ""),
                        'compétences': client.get('compétences', []),
                        'secteur': client.get('secteur', "")
                    }
                    similar_clients.append(similar_client_info)

            similar_clients_profiles.append(similar_clients)

        return jsonify(similar_clients), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400


if __name__ == '__main__':
    app.run(debug=True)
